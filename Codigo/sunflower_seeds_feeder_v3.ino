#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

Adafruit_PWMServoDriver servos = Adafruit_PWMServoDriver(0x40);

unsigned int pos0=172; // ancho de pulso en cuentas para pocicion 0°
unsigned int pos180=565; // ancho de pulso en cuentas para la pocicion 180°

unsigned long previousMillis = 0;
unsigned long currentMillis = 0;

const int RED_Button = 13;

const long slow = 1;
float dx = 0.1;

unsigned long t0 = 0;
unsigned long t = 0;
float x = 0;
float fx = 0;

int offset_servo1 = -5;
int offset_servo2 = -15;
int offset_servo3 = 0;

void setup() {
  servos.begin();  
  servos.setPWMFreq(60); //Frecuecia PWM de 60Hz o T=16,66ms
  pinMode(RED_Button,INPUT);
}

void loop() {

    //Home position
    setServo(1,90+offset_servo1);
    setServo(2,90+offset_servo2);
    setServo(3,210+offset_servo3);

    if(digitalRead(RED_Button) == HIGH){
      feed();
    }
    delay(10);
    
}

void setServo(uint8_t n_servo, float angulo) {
  int duty;
  duty=map(angulo,0,180,pos0, pos180);
  servos.setPWM(n_servo, 0, duty);  
}

void feed() {
   
    //Home position
    setServo(1,90+offset_servo1);
    setServo(2,90+offset_servo2);
    setServo(3,210+offset_servo3);
    
    delay(1000);

    for(x=90; x<=110; x=x+dx){
      setServo(1,x+offset_servo1);
      setServo(2,90+offset_servo2);
      setServo(3,210+offset_servo3);
      delay(slow);
    }
    
    delay(300);
    for(x=90; x<=120; x=x+dx){
      setServo(1,x+20+offset_servo1);
      setServo(2,x+offset_servo2);
      setServo(3,210+offset_servo3);
      delay(slow);
    }
    delay(300);
    for(x=120; x<=140; x=x+dx){
      setServo(1,140+offset_servo1);
      setServo(2,x+offset_servo2);
      setServo(3,210+offset_servo3);
      delay(slow);
    }
    delay(500);

    for(x=140; x>=50; x=x-dx){
      setServo(1,x+offset_servo1);
      setServo(2,x+offset_servo2);
      setServo(3,210+offset_servo3);
      delay(slow);
    }
    
    delay(500);

    for(x=210; x>=40; x=x-2*dx){
      setServo(1,50+offset_servo1);
      setServo(2,50+offset_servo2);
      setServo(3,x+offset_servo3);
      delay(slow);
    }

    delay(500);

    for(x=50; x<=100; x=x+dx){
      setServo(1,x+offset_servo1);
      setServo(2,x+offset_servo2);
      setServo(3,40+offset_servo3);
      delay(slow);
    }
    
    delay(3000);

     for(x=100; x>=90; x=x-2*dx){
      setServo(1,x+offset_servo1);
      setServo(2,x+offset_servo2);
      setServo(3,40+offset_servo3);
      delay(slow);
    }
    
    for(x=40; x<=210; x=x+2*dx){
      setServo(1,90+offset_servo1);
      setServo(2,90+offset_servo2);
      setServo(3,x+offset_servo3);
      delay(slow);
    }
    delay(1000);
}
