# Alimentador Automatico - Open Source


## Si no has visto el video, ve a verlo! - Haz click en la imagen

[![Robot que te alimenta en YouTube](Imágenes/Video%20YT.jpg)](https://www.youtube.com/watch?v=TlcM9L2CzjM&ab_channel=FuntronicsbyTechmake)

Te invitamos a que nos ayudes a mejorar el prototipo del robot alimentador.

Ten la libertad de mejorar cualquiera de las areas: Codigo, diseño mecanico, ergonomico, etc.

Y te animamos a que compartas tus avances con los demas (aqui o por el medio que tu prefieras).

¡Gracias por colaborar!
